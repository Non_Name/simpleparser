#include "simple_parser.hpp"

#include <algorithm>
#include <vector>

uint8_t getchecksum(std::vector<uint8_t>::const_iterator begin, std::vector<uint8_t>::const_iterator end)
{
    int whole_checksum = 0;
    uint8_t checksum;

    std::for_each(begin, end, [&whole_checksum, &checksum] (uint8_t n) {
        whole_checksum = whole_checksum + n;
        checksum = ~(0xFF & whole_checksum) + 1;
    });

    return checksum;
}

Packet::Packet(PacketFormat packet_format) : m_packet_format{packet_format}
{}

PolPacket::PolPacket(PacketFormat packet_format) : Packet{packet_format} 
{}

void PolPacket::push_byte(uint8_t byte)
{
    m_packet_format.checksum = byte;
}

std::string PolPacket::to_string() const
{
    std::string result = "type:POLL,";
    result += m_packet_format.address.property + std::to_string(static_cast<unsigned int>(m_packet_format.address.value)) + ",";
    result += m_packet_format.sqn.property + std::to_string(static_cast<unsigned int>(m_packet_format.sqn.value));
    return result; 
}

BuzPacket::BuzPacket(PacketFormat packet_format) : Packet(packet_format)
{}

void BuzPacket::push_byte(uint8_t byte)
{
    if(m_counter < buz_args.size())
    {
        m_args.emplace_back(ByteType{buz_args[m_counter], byte});
        m_counter++;
        return;
    }
    m_packet_format.checksum = byte;
}

std::string BuzPacket::to_string() const
{
    std::string result = "type:BUZ,";
    result += m_packet_format.address.property + std::to_string(static_cast<unsigned int>(m_packet_format.address.value)) + ",";
    result += m_packet_format.sqn.property + std::to_string(static_cast<unsigned int>(m_packet_format.sqn.value)) + ",";
    size_t arg_size = m_args.size();
    for(size_t i = 0; i < arg_size; i++)
    {
        result += m_args[i].property + std::to_string(static_cast<unsigned int>(m_args[i].value));
        result += (i == arg_size - 1)? "" : ",";
    }
    return result;
}

void SimpleParser::push(uint8_t byte)
{
    if(m_start_flag)
    {
        switch(counter)
        {
            case 1:
                if(byte >= 0x00 && byte <= 0x7E)
                    m_packet_format.address.value = byte;
                else
                    reset();
                break;
            case 2:
                m_packet_format.packet_size = static_cast<uint16_t>(byte);
                break;
            case 3:
                m_packet_format.packet_size |= (static_cast<uint16_t>(byte) << 8); // transform 2 uint8_t -> 1 uint16_t (high, low)
                break;
            case 4:
                m_packet_format.sqn.value = byte;
                break;
            case 5:
                switch(byte)
                { 
                case POLL:
                    m_packet = std::make_shared<PolPacket>(m_packet_format);
                    break;
                case BUZ:
                    m_packet = std::make_shared<BuzPacket>(m_packet_format);
                    break;
                default:
                    reset(); 
                }
                break;
            default:
                m_packet->push_byte(byte);
        }
        counter++;
        if(m_packet_format.packet_size != 0x0000 && counter == m_packet_format.packet_size) // bad way
        {
           uint8_t checksum = getchecksum(m_bytes.cbegin(), m_bytes.cend()); 
           if(checksum != byte)
           {
                reset();
           }
        }
        m_bytes.push_back(byte);    
    }
    else if(byte == START)
    {
        m_bytes.push_back(byte);
        counter++;
        m_start_flag = true;
    }
}

std::shared_ptr<Packet> SimpleParser::get_packet() const
{
    return m_packet;
}

void SimpleParser::reset()
{
    counter = 0;
    m_packet = nullptr;
    m_start_flag = false;
    m_bytes.clear();
}


// TODO: