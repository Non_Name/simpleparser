# Simple Parser

## Technology Stack

- GTest
- C++
- CMake

## Build & Run

```
mkdir build && cd build
cmake ..
make
./parser_test
```
## Results

All tests are passed on the Ubuntu Linux 22.04
