#ifndef SIMPLE_PARSER_HPP
#define SIMPLE_PARSER_HPP

#include <iostream>
#include <cstdint>
#include <memory>
#include <vector>
#include <array>

constexpr uint8_t POLL = 0x60;
constexpr uint8_t BUZ = 0x6A;
constexpr uint8_t START = 0x53;
const std::array<std::string, 5> buz_args = {"reader:", "tone:", "on:", "off:", "count:"};

struct ByteType
{
    std::string property;
    uint8_t value;
};

struct PacketFormat
{
    uint16_t packet_size{0x0000}; //union of low and high size bytes
    uint8_t packet_type{0x00};
    uint8_t start_code{START};
    uint8_t checksum{0x00};
    ByteType address{"addr:", 0x00};
    ByteType sqn{"sqn:", 0x00};
};

/* This is one of the several ways how to perform BuzPacket data args
struct BuzParams
{   // default values //
    ByteType read_num{"reader:", 0x00};
    ByteType tone_code{"tone:", 0x00};
    ByteType on_time{"on:", 0x23};
    ByteType off_time{"off:", 0x14};
    ByteType count{"count:", 0x01};
};
*/

class Packet
{
public:
    Packet(PacketFormat packet_format);
    virtual std::string to_string() const = 0;
    virtual void push_byte(uint8_t byte) = 0;
protected:
    PacketFormat m_packet_format;
};

class PolPacket : public Packet
{
public:
    PolPacket(PacketFormat packet_format);
    PolPacket() = delete;
    virtual std::string to_string() const override;
    virtual void push_byte(uint8_t byte) override;
};

class BuzPacket : public Packet
{
public:
    BuzPacket(PacketFormat packet_format);
    BuzPacket() = delete;
    virtual std::string to_string() const override;
    virtual void push_byte(uint8_t byte) override;
private:
    std::vector<ByteType> m_args;
    size_t m_counter{0};
};

class SimpleParser
{
public:
    void push(uint8_t b);
    void reset();
    std::shared_ptr<Packet> get_packet() const;
private:
    bool m_start_flag{false};
    std::shared_ptr<Packet> m_packet{nullptr};
    std::vector<uint8_t> m_bytes;
    size_t counter{0};
    PacketFormat m_packet_format;
};

#endif // SIMPLE_PARSER_HPP